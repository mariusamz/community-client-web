(function(){
    angular
        .module('component-footer')
        .directive('footer', footer);

    function footer(){
        return {
            restrict: 'AE',
            templateUrl : 'js/components/footer/footer.html'
        };
    }    
})();