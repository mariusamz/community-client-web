(function(){
    angular
        .module('component-notifications-menu')
        .directive('notificationsMenu', notificationsMenu);

    function notificationsMenu(){
        return {
            restrict: 'E',
            replace: true,
            templateUrl : 'js/components/notifications-menu/notifications-menu.html',
            link: function (scope){
            }
        };
    }
})();