(function(){
    
    angular
        .module('component-action-menu')
        .directive('actionmenu', actionMenu);

    actionMenu.$inject = ['$window', '$location'];

    function actionMenu($window, $location){
        return {
            restrict: 'AE',
            scope: {
                items: '=',
                hideSearch: '@',
                hideAsk: '@',
                query: '='
            },
            templateUrl : 'js/components/action-menu/action-menu.html',
            link: function (scope){
                // Handle Search and Ask events.
                scope.search = search;
                scope.goToAskQuestion = goToAskQuestion;

                scope.menuItems = [];
                scope.searchQuery = {
                    term: null
                };
                scope.fixMenu = false;
                var windowEl = angular.element($window);


                // Populate menu watcher
                scope.$watch('items', function(data){
                    scope.menuItems = data;
                });

                if(scope.query){
                    scope.searchQuery.term = scope.query;
                }

                // Hide Search box and/or Ask button completely based on the parent page.
                if (angular.isUndefined(scope.hideSearch)) {
                    scope.hideSearch = false;
                }
                if (angular.isUndefined(scope.hideAsk)) {
                    scope.hideAsk = false;
                }

                function search(){
                    if(scope.searchQuery.term){
                        $location.search({});
                        $location.url('/questions/search?query=' + scope.searchQuery.term);
                    }
                }

                function goToAskQuestion(){
                    $location.search({});
                    $location.path('/questions/ask');
                }

                // Fix menu to top when user scrolls
                var handler = function() {
                    var distanceFromTop = windowEl.scrollTop();
                    if(distanceFromTop >= 80){
                        scope.fixMenu = true;
                    } else {
                        scope.fixMenu = false;
                    }
                };

                // Detach handler from window object. 
                var cleanUp = function () {
                    windowEl.off('scroll', handler);
                };

                // Events
                windowEl.on('scroll', scope.$apply.bind(scope, handler));
                scope.$on('$destroy', cleanUp);
            }
        };
    }

})();