(function(){

    angular
        .module('component-search-main')
        .directive('searchmain', searchMain);

    searchMain.$inject = ['$location'];

    function searchMain($location){
        return {
            restrict: 'E',
            scope: {
                query: '='
            },
            templateUrl : 'js/components/search-main/search-main.html',
            link: function (scope){

                if(scope.query){
                    scope.item = scope.query;
                }
                scope.search = search;
                scope.goToAskQuestion = goToAskQuestion;

                function search(){
                    if(scope.item){
                        $location.search({});
                        $location.url('/questions/search?query=' + scope.item);
                    }
                }

                function goToAskQuestion(){
                    $location.search({});
                    $location.path('/questions/ask');
                }
            }
        };
    }

})();