(function(){
    angular
        .module('component-navbar')
        .directive('navbar', navbar);

    navbar.$inject = ['ENV', 'currentUserService'];

    function navbar(ENV, currentUserService){
        return {
            restrict: 'AE',
            templateUrl : 'js/components/navbar/navbar.html',
            link: function (scope){

                scope.notificationsExist = false;

                scope.currentUser = currentUserService.currentUser();
                scope.$on('currentUser:updated', function(event,data) {
                    scope.currentUser = data;
                });

                scope.coursesUrl = ENV.campusUrl + 'library';
                scope.legacyCommunityUrl = ENV.legacyCommunityUrl;
                scope.supportUrl = ENV.supportUrl;
                scope.accountUrl = ENV.campusUrl + 'account';
                scope.signOutUrl = ENV.campusUrl + 'logout';
            }
        };
    }
})();