(function(){
    angular
        .module('notification')
        .directive('notification', notification);

    notification.$inject = ['$rootScope', '$timeout'];

    function notification($rootScope, $timeout) {
        return {
            restrict: 'AE',
            templateUrl: 'js/components/notifications/notification.html',
            link: link
        };

        function link(scope, element, attr){

            scope.notifications = [];

            $rootScope.$on('notification:add',function(e, notification){
                scope.notifications.push(notification);
                if(notification.duration){
                    $timeout(function(){
                        var index = scope.notifications.indexOf(notification);
                        scope.notifications.splice(index,1);
                    },notification.duration);
                }
            });
        }
    }
})();