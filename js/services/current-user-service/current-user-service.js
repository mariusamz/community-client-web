(function() {
    angular
        .module('communityAPI')
        .factory('currentUserService', currentUserService);

    currentUserService.$inject = ['$cookies', '$rootScope', 'ENV'];

    function currentUserService($cookies, $rootScope, ENV) {
        var currentUser;

        return{
            currentUser : getCurrentUser,
            setCurrentUser : setCurrentUser,
            removeCurrentUser : removeCurrentUser
        };

        function setCurrentUser(memberId, username, firstName, lastName, avatarUrl) {
            currentUser = {
                memberId: memberId,
                username: username,
                firstName: firstName,
                lastName: lastName,
                avatarUrl: avatarUrl
            };

            $cookies.put('username', currentUser.username);
            $cookies.put('firstname', currentUser.firstName);
            $cookies.put('lastname', currentUser.lastName);
            $cookies.put('image', currentUser.avatarUrl);

            $rootScope.$broadcast('currentUser:updated', currentUser);
        }

        function getCurrentUser() {

            // Ensure current user exists and has a username
            if(currentUser && currentUser.username) {
                return currentUser;
            }

            var memberInfo;

            // When parsing fails it means we do not have a token, we need to log out then
            try {
                memberInfo = JSON.parse(decodeURIComponent($cookies.get('member_info')));
            }
            catch (error){
                window.location.href = ENV.campusUrl + 'logout';
            }


            currentUser = {
                memberId: $cookies.get('member_id'),
                username: memberInfo.username,
                firstName: memberInfo.firstname,
                lastName: memberInfo.lastname,
                avatarUrl: memberInfo.image
            };


            console.log('Current username', currentUser);

            if(!currentUser.username){
                currentUser = null;
                window.location.href = ENV.campusUrl + 'logout';
            }
            return currentUser;
        }

        /**
         * This is temp for now, clears the cookies, sets currentUser to null and broadcasts
         */
        function removeCurrentUser() {

            $cookies.remove('member_id');
            $cookies.remove('username');
            $cookies.remove('firstname');
            $cookies.remove('lastname');
            $cookies.remove('image');
            $cookies.remove('member_info');

            currentUser = null;

            $rootScope.$broadcast('currentUser:updated', currentUser);
        }

    }
})();