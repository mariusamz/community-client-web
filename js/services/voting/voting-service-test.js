describe('Voting Service ', function () {
    var votingService, httpBackend;

    beforeEach(module('communityAPI'));

    beforeEach(inject(function($injector) {
        votingService = $injector.get('votingService');
        httpBackend = $injector.get('$httpBackend');
    }));

    var response = {
        content: {
            id: '1234'
        }
    };

    var voteValue = 1,
        questionId = 2,
        answerId = 2;

    var expectedMsg = 'We’re sorry. The server was unavailable. Please try again.';


    it("should successfully vote", function () {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/2/vote')
            .respond(201, response);

        votingService.vote(questionId, answerId, voteValue)
            .then(function(response){
                expect(response).toBeDefined();
            });

        httpBackend.flush();
    });

    it("should execute catch if error is return form API", function () {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions/2/answers/2/vote')
            .respond(503, {});

        votingService.vote(questionId, answerId, voteValue)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBeDefined();
                expect(response).toBe(expectedMsg);
            });

        httpBackend.flush();
    });

    it("should reject vote if value is not within the range", function () {

        votingService.vote(questionId, answerId, 2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBeDefined();
            });

        votingService.vote(questionId, answerId, 3)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBeDefined();
            });

        votingService.vote(questionId, answerId, -2)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBeDefined();
            });

        votingService.vote(questionId, answerId, -3)
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBeDefined();
            });

    });


    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

});