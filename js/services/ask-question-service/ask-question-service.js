(function (){
    angular
        .module('communityAPI')
        .service('askQuestionService', askQuestionService);

    askQuestionService.$inject = ['$http', '$q', 'ENV', 'errorService'];

    function askQuestionService($http, $q, ENV, errorService) {

        var askQuestionEndpoint = ENV.apiEndpoint + 'questions';


        return{
            postQuestion : postQuestion
        };

        //////////////////////////////

        /**
         * Post Question to the API
         * @param data, question data
         * @returns {Promise<U>}
         */
        function postQuestion(data){
            return $http
                .post(askQuestionEndpoint, data)
                .then(onPostSuccess)
                .catch(onPostError);

            function onPostSuccess(response){
                return response.data.content.id;
            }

            function onPostError(response){
                return errorService.parseRejectPromise(response);
            }
        }
        
    }
})();