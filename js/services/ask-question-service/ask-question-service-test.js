describe('Ask Question Service ', function () {
    var askQuestionService, httpBackend;

    beforeEach(module('communityAPI'));

    beforeEach(inject(function($injector) {
        askQuestionService = $injector.get('askQuestionService');
        httpBackend = $injector.get('$httpBackend');
    }));

    var response = {
        content: {
            id: '1234'
        }
    };

    var expectedMsg = 'We’re sorry. The server was unavailable. Please try again.';


    it("should post a question and receive a valid id", function () {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions')
            .respond(response);

        askQuestionService.postQuestion()
            .then(function(response){
                expect(response).toBe('1234');
            });
        
        httpBackend.flush();
    });

    it("should post a question and execute a catch block", function () {
        httpBackend
            .when('POST', 'https://questionsapi.amzingsandbox.com/api/questions')
            .respond(503, {});

        askQuestionService.postQuestion()
            .then(function(response){
                return false;
            })
            .catch(function(response){
                expect(response).toBe(expectedMsg);
            });

        httpBackend.flush();
    });

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

});