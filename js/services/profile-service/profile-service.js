(function(){
    angular
        .module('communityAPI')
        .service('profileService', profileService);

    profileService.$inject = ['$http', 'ENV', 'errorService'];

    function profileService($http, ENV, errorService) {

        return{
            getProfileContent : getProfileContent
        };

        function getProfileContent(username) {
            var profileEndpoint = ENV.apiEndpoint + 'user/' + username + '/profile';

            return $http
                .get(profileEndpoint)
                .then(onGetQuestionsSuccess)
                .catch(onGetQuestionsError);

            function onGetQuestionsSuccess(response) {
                return response.data.content;
            }

            function onGetQuestionsError(response) {
                return errorService.parseRejectPromise(response);
            }
        }

    }
})();