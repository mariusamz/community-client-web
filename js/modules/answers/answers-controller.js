(function(){
    /*global angular*/

    angular
        .module('answers')
        .controller('answersCtrl', Answers);

    Answers.$inject = ['tagsService', '$stateParams', 'questionService', 'answersService', 'votingService', '$location'];

    function Answers(tagsService, $stateParams, questionService, answersService, votingService, $location) {
        'use strict';

        var vm = this;
        var questionId;

        vm.vote = vote;
        vm.postAnswer = postAnswer;
        vm.showAnswerBox = showAnswerBox;
        vm.cancel = cancel;
        vm.onBlur = onBlur;
        vm.getTooltipText = getTooltipText;
        vm.acceptAnswer = acceptAnswer;
        vm.canUserAcceptAnswer = canUserAcceptAnswer;
        vm.tagClick = onTagClick;
        vm.searchTag= searchTag;

        vm.answerMsg = '';
        vm.loading = false;
        vm.answerMaxLength = 10000;

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };

        ////////////////////////////////////

        init();

        function init(){
            questionId = $stateParams.QuestionId;


            tagsService
                .getTags()
                .then(function(data){
                    vm.suggestedWords = data.data.tags;
                });

            questionService
                .getQuestionWithAnswers(questionId)
                .then(updateView)
                .catch(onRequestError);

            function updateView(response){
                vm.question = response[0];
                vm.answers = response[1];
            }
        }


        /**
         * If current user that is logged in
         * has already voted an answer call un vote function else vote up.
         * We update the vote value before API returns success so
         * we do not have to display any loading state on votes. We also
         * need to lock the vote on the answer to prevent user from
         * clicking and sending multiply API calls. Vote is unlocked once the API
         * successfully updates votes record.
         * @param answer
         */
        function vote(answer){

            if(answer.votes.locked){
                return;
            }

            if(answer.votes.hasCurrentUserVoted){
                unVote(answer);
            }
            else{
                voteUp(answer);
            }

        }

        function voteUp(answer){
            var originalIndex = vm.answers.indexOf(answer);
            vm.answers[originalIndex].votes.count += 1;
            vm.answers[originalIndex].votes.hasCurrentUserVoted = true;
            vm.answers[originalIndex].votes.locked = true;


            var voteValue = '+1';
            votingService
                .vote(questionId, answer.id, voteValue)
                .then(updateAnswer)
                .catch(onRequestError);

            function updateAnswer(){
                vm.answers[originalIndex].votes.locked = false;
            }
        }


        function unVote(answer){
            var originalIndex = vm.answers.indexOf(answer);
            vm.answers[originalIndex].votes.count -= 1;
            vm.answers[originalIndex].votes.hasCurrentUserVoted = false;
            vm.answers[originalIndex].votes.locked = true;


            var voteValue = '0';
            votingService
                .vote(questionId, answer.id, voteValue)
                .then(updateAnswer)
                .catch(onRequestError);

            function updateAnswer(){
                vm.answers[originalIndex].votes.locked = false;
            }
        }


        /**
         * Accepts an answer as selected answer. We need to mark it as accepted
         * so the UI updates accordingly. 
         * @param answer
         */
        function acceptAnswer(answer){
            var originalIndex = vm.answers.indexOf(answer);
            vm.answers[originalIndex].accepted = 1;
            vm.question.hasAcceptedAnswer = 1;


            var acceptValue = '1';
            answersService
                .acceptAnswer(questionId, answer.id, acceptValue)
                .then()
                .catch(onRequestError);
        }
        

        /**
         * Post answer to a question.
         * We only need the details of the answer,
         * JWT token contains user information. Once the question is posted
         * its ID is returned, we then use that ID to retrieve the answer object
         * with all the details.
         */
        function postAnswer(){
            if(vm.loading){
                return;
            }
            vm.loading = true;
            
            var data = {
                text: ''
            };
            data.text = vm.answerMsg;

            answersService
                .postAnswer(questionId, data)
                .then(retrieveAnswer)
                .catch(onRequestError);

            function retrieveAnswer(answerId){
                answersService
                    .getAnswerById(questionId,answerId)
                    .then(updateView)
                    .catch(onRequestError);
            }

            function updateView(answer){
                answer.shouldScroll = true;
                vm.answers.push(answer);
                vm.loading = false;
                vm.expandAnswerBox = false;
                vm.answerMsg = '';
            }
        }

        function getTooltipText(answer){
            if(answer.votes.hasCurrentUserVoted){
                return 'Unvote';
            }
            else{
                return 'Vote For This Answer';
            }
        }

        /**
         * We need to check if currently logged in user can accept an answer,
         * (that means currently logged in user created the question) 
         * and that the question can be marked as having an accepted answer
         * i.e user that created the question did not accept an answer yet. 
         * @returns {boolean}
         */
        function canUserAcceptAnswer(){
            return vm.question.currentUserCanAcceptSolution === 1 && vm.question.hasAcceptedAnswer === 0;
        }

        function cancel(){
            vm.expandAnswerBox = false;
        }

        function showAnswerBox(){
            vm.expandAnswerBox = true;
        }

        function onRequestError(msg){
            vm.errorMsg = msg;
        }

        function onBlur(){
            vm.showInlineError = true;
        }

        function onTagClick(e){
            // We dont want the # symbol so we strip it here
            var tag = e.target.innerText.substring(1);
            $location.url('/questions/search?query=' + tag);
        }

        function searchTag(tag){
            $location.url('/questions/search?query=' + tag);
        }

    }
})();