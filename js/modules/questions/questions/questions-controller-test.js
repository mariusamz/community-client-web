// describe('Questions Controller', function () {
//
//     beforeEach(module('questions'));
//     beforeEach(module('communityAPI'));
//     beforeEach(module('LocalStorageModule'));
//
//     var scope,
//         vm,
//         $q,
//         deferred,
//         stateParams,
//         httpBackend;
//
//     var response = {
//         content: [
//             {title:'Test1'},
//             {title:'Test2'},
//             {title:'Test3'}
//         ]
//     };
//
//     beforeEach(inject(function (_$rootScope_,$controller, _$q_, questionService, $httpBackend, currentUserService) {
//         scope = _$rootScope_.$new();
//         $q = _$q_;
//         deferred = _$q_.defer();
//         httpBackend = $httpBackend;
//
//         // Use a Jasmine Spy for mocking
//         // spyOn(questionService, 'getActiveQuestions').and.returnValue(deferred.promise);
//
//         // Init controller with mocked dependencies
//         vm = $controller('questionsCtrl', {
//             $scope: scope,
//             questionService: questionService,
//             currentUserService: currentUserService
//         });
//
//     }));
//
//     it('should have initialized scope vars correctly', function() {
//         expect(vm).toBeDefined();
//         expect(vm.localTabsData).toBeDefined();
//     });
//
//     it('should call questions api', function() {
//
//         httpBackend
//             .when('GET', 'http://ec2-54-90-137-61.compute-1.amazonaws.com:8888/api/questions?mode=active')
//             .respond(response);
//
//
//         scope.$apply();
//         httpBackend.flush();
//         expect(vm.questions.length).toBe(3);
//
//     });
//
//
// });
