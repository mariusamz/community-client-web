(function(){
    /*global angular*/

    angular
        .module('questions')
        .controller('questionsCtrl', Questions);

    Questions.$inject = ['questionService', '$location', 'currentUserService'];

    function Questions(questionService, $location, currentUserService) {
        'use strict';

        var vm = this;
        var sort;
        var currentUser = currentUserService.currentUser();

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };
        
        vm.localTabsData = {
            heading: 'ACTIVE QUESTIONS',
            items: {
                active: {
                    active: true,
                    link: 'questions?sort=active',
                    text: 'Active'
                },
                unanswered: {
                    active: false,
                    link: 'questions/unanswered',
                    text: 'Unanswered'
                },
                my: {
                    active: false,
                    link: 'questions/user/' + currentUser.username,
                    text: 'My Questions'
                }
            }
        };
        
        ////////////////////////////////////

        init();

        function init(){
            sort = $location.search().sort;

            if(!sort){
                sort = 'active';
            }

            questionService
                .getQuestions(sort)
                .then(updateView)
                .catch(onRequestError);

            function updateView(response){
                vm.questions = response;

            }
        }

        function onRequestError(msg){
            vm.errorMsg = msg;
        }

    }
})();