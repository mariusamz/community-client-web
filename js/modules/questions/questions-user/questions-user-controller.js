(function(){
    /*global angular*/

    angular
        .module('questions')
        .controller('userQuestionsCtrl', UserQuestions);

    UserQuestions.$inject = ['questionService', '$stateParams', 'currentUserService'];

    function UserQuestions(questionService, $stateParams, currentUserService) {
        'use strict';

        var vm = this;
        var currentUser = currentUserService.currentUser();

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };

        vm.localTabsData = {
            heading: 'MY QUESTIONS',
            items: {
                active: {
                    active: false,
                    link: 'questions?sort=active',
                    text: 'Active'
                },
                unanswered: {
                    active: false,
                    link: 'questions/unanswered',
                    text: 'Unanswered'
                },
                my: {
                    active: true,
                    link: 'questions/user/' + currentUser.username,
                    text: 'My Questions'
                }
            }
        };

        ////////////////////////////////////

        init();

        function init(){
            questionService
                .getQuestionsByUser($stateParams.Username)
                .then(updateView)
                .catch(onRequestError);

            function updateView(response){
                vm.questions = response;
            }
        }

        function onRequestError(msg){
            vm.errorMsg = msg;
        }

    }
})();