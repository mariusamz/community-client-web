describe('Ask Question Controller', function () {

    beforeEach(module('ask-question'));
    beforeEach(module('communityAPI'));
    beforeEach(module('ui.bootstrap'));
    
    var scope,
        vm,
        $q,
        deferred,
        location;

    // Mock Mixpanel
    function MixPanelMock() {
        this.track = track;
        function track(eventString) {
            // Do nothing here    
        }
    }
    window.mixpanel = new MixPanelMock();

    beforeEach(inject(function (_$rootScope_,$controller, _$q_, $location,tagsService, $uibModal, askQuestionService) {
        scope = _$rootScope_.$new();
        $q = _$q_;
        location = $location;
        deferred = _$q_.defer();

        // Use a Jasmine Spy for mocking
        spyOn(tagsService, 'getTags').and.returnValue(deferred.promise);
        spyOn(askQuestionService, 'postQuestion').and.returnValue(deferred.promise);
        spyOn($uibModal, 'open').and.returnValue('any');
        
        
        // Init controller with mocked dependencies 
        vm = $controller('askCtrl', { 
            $scope: scope,
            $uibModal : $uibModal,
            askQuestionService: askQuestionService,
            tagsService: tagsService
        });
        
        
    }));

    it('should have initialize scope vars correctly', function() {
        expect(vm.titleError).toBe(false);
        expect(vm.loading).toBe(false);
        expect(vm.showTagsErrorAlert).toBe(false);
        expect(vm.showTagsWarningAlert).toBe(false);
    });

    it('should post question if all fields are correct', function() {
        vm.questionTitle = 'This is a test title';
        vm.addedTagsDescription = ['#descrition'];
        vm.addedTagsTitle = ['#title'];
        scope.$apply();
        vm.postAnswer();
        expect(vm.loading).toBe(true);
    });

    it('should not post question if title is less then expected', function() {
        vm.questionTitle = 'Test';
        scope.$apply();
        var result = vm.postAnswer();
        expect(vm.titleError).toBe(true);
        expect(result).toBeFalsy();
    });

    it('should post question if there is no tags', function() {
        vm.addedTagsDescription = [];
        vm.addedTagsTitle = [];
        vm.questionTitle = 'Test with correct title';
        scope.$apply();
        vm.postAnswer();
        expect(vm.loading).toBe(true);
    });

    it('should not post question if there more than 20 tags', function() {
        vm.addedTagsDescription = ['1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'];
        vm.addedTagsTitle = ['2'];
        vm.questionTitle = 'Test with correct title';
        scope.$apply();
        var result  = vm.postAnswer();
        expect(result).toBeFalsy();
        expect(vm.showTagsErrorAlert).toBe(true);
    });

    it('should set field focus', function() {
        vm.setFieldFocus();
        scope.$apply();
        expect(vm.focusField).toBeTruthy();
    });

    it('should redirect user if cancel button is pressed', function() {
        vm.cancel();
        scope.$apply();
        expect(location.path()).toBe('/');
    });

    it('should show prompt initially', function() {
        expect(vm.showPrompt).toBe(true);
    });

    it('should hide prompt if user closes it', function() {
        vm.closePrompt();
        scope.$apply();
        expect(vm.showPrompt).toBe(false);
    });

});
