(function(){
    /*global angular*/

    angular
        .module('search-questions')
        .controller('searchQuestionsCtrl', SearchQuestions);

    SearchQuestions.$inject = ['questionService', '$location'];

    function SearchQuestions(questionService, $location) {
        'use strict';

        var vm = this;
        var query;

        vm.actionMenuList= {
            questions: {
                display: true,
                active: true,
                link: 'questions',
                text: 'Questions'
            }
        };


        ////////////////////////////////////

        init();

        function init(){
            query = $location.search().query;
            vm.query = query;

            questionService
                .searchQuestions(query)
                .then(updateView)
                .catch(onRequestError);

            function updateView(response){
                vm.questions = response;
            }
        }

        function onRequestError(msg){
            vm.errorMsg = msg;
        }

    }
})();