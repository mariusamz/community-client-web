(function(){
    /*global angular*/

    angular
        .module('profile')
        .controller('profileCtrl', Profile);

    Profile.$inject = ['$uibModal', '$rootScope', '$location', '$scope', 'profileService', '$stateParams'];

    function Profile($uibModal, $rootScope, $location, $scope, profileService, $stateParams) {
        'use strict';

        var vm = this;

        // Scope vars
        vm.profileData = '';
        vm.errorMsg = '';

        /////////////////

        init();

        function init() {
            profileService.getProfileContent($stateParams.Username)
                .then(updateView)
                .catch(onRequestError);

            function updateView(response) {
                vm.profileData = response;
            }

            function onRequestError(msg){
                vm.errorMsg = msg;
            }
        }
    }
})();