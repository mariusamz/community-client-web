(function(){
    angular
        .module('scrollTo',[])
        .directive('scrollToView', scrollToView);

    scrollToView.$inject = ['$timeout'];

    function scrollToView($timeout){
        return function(scope, element, attrs) {

            scope.$watch(attrs.scrollToView, function(value) {
                if (value) {
                    $timeout(function(){
                        var padding = 50;
                        var pos = element[0].getBoundingClientRect();
                        $('html, body').animate({
                            scrollTop: pos.top - padding
                        }, 1000);
                    },100);
                }
            });
        };
    }

})();
