(function(){
    angular
        .module('ask-question')
        .controller('ModalInstanceCtrl', modalInstanceCtrl);

    modalInstanceCtrl.$inject = ['$uibModalInstance'];

    function modalInstanceCtrl($uibModalInstance) {
        var $ctrl = this;
        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();