(function(){
    angular
        .module('hashtagify',[])
        .directive('hashtagify', ['$timeout', '$compile',
            function($timeout, $compile) {
                return {
                    restrict: 'A',
                    scope: {
                        uClick: '&userClick',
                        tClick: '&termClick',
                        bindModel: '=ngModel'
                    },
                    link: function(scope, element, attrs) {

                         scope.$watch('bindModel', function(newVal){
                            if(newVal){
                                replaceText(newVal);
                            }
                        }, true);

                        function replaceText(text){
                            var newHtml = text;
                            newHtml = newHtml.replace(/(|\s)*@(\w+)/g, '$1<a ng-click="uClick({$event: $event})" class="hashtag">@$2</a>');

                            newHtml = newHtml.replace(/(^|\s)*#(\w+)/g, '$1<a ng-click="tClick({$event: $event})" class="hashtag">#$2</a>');


                            element.html(newHtml);

                            $compile(element.contents())(scope);
                        }
                    }
                };
            }
        ]);

})();