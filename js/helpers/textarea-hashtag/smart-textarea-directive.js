(function(){
    angular
        .module('areahashtag',[])
        .directive('smartTextArea', smartTextArea);

    function smartTextArea() {
        return {
            restrict: 'A',
            scope: {
                words: '=',
                addedTag: '&',
                bindModel: '=ngModel'
            },
            link: function(scope, element) {
                element.hashtags();


                // Start autocomplete only when we get suggested tags from the controller
                scope.$watch('words', function(newVal){
                    if(newVal){
                        startAutocomplete(newVal);
                    }
                }, true);

                //Watch value of text area, parse the text and get array of tags
                scope.$watch('bindModel',function(newVal){
                    if(newVal){
                        var tags = newVal.match(/[#|@](([a-zA-Z0-9\-]+)|([\u0600-\u06FF]+))/g);
                        if(tags){
                            element.trigger('keyup');
                            scope.addedTag({tag:tags});
                        }
                        else{
                            scope.addedTag({tag:[]});
                        }
                    }
                }, true);

                function startAutocomplete(words){
                    element.textcomplete([
                        {
                            words: words,
                            match: /([@|#][A-Za-z0-9_\-]*)$/,
                            search: function (term, callback) {
                                callback($.map(this.words, function (word) {
                                    return word.toLowerCase().indexOf(term.toLowerCase()) === 0 ? word : null;
                                }));
                            },
                            index: 1,
                            replace: function (word) {
                                return word + ' ';
                            }
                        }
                    ], {
                        onKeydown: function (e, commands) {
                            if (e.ctrlKey && e.keyCode === 74) {
                                return commands.KEY_ENTER;
                            }
                        }
                    });
                }

            }
        };
    }
})();