/*jshint camelcase: false */
module.exports = function (grunt) {

    // Load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            // if any .less file changes in directory 'css/less/' run the 'less'-task.
            files: 'css/less/*.less',
            tasks: ['less', 'notify:lessMsg']
        },
        // 'less'-task configuration
        less: {
            development: {
                options: {
                    // Specifies directories to scan for @import directives when parsing.
                    paths: ['css/less/']
                },
                files: {
                    // compilation.css  :  source.less
                    'css/app.css': 'css/app.less'
                }
            }
        },
        notify: {
            lessMsg: {
                options: {
                    title: 'Less',
                    message: 'Less compilation succesfully finished!'
                }
            }
        },
        jshint: {
            all: ['Gruntfile.js',
                'js/modules/**/*.js',
                'js/components/**/*.js',
                'js/helpers/**/*.js',
                'js/services/*.js',
                'tests/integration/*.js'],
            options: {
                jshintrc: '.jshintrc'
            }
        },
        protractor: {
            options: {
                configFile: 'tests/conf.js',
                keepAlive: true,
                noColor: false,
                args: {
                    // Arguments passed to the command
                }
            },
            e2e: {
                options: {
                    keepAlive: true
                }
            }
        },
        //start the selenium server before executing integration tests
        protractor_webdriver: {
            start: {
                options: {
                    command: 'webdriver-manager start'
                }
            }
        },
        karma: {
            unit: {
                options: {
                    frameworks: ['jasmine'],
                    singleRun: true,
                    browsers: ['PhantomJS'],
                    files: [
                        'bower_components/angular/angular.js',
                        'bower_components/angular-mocks/angular-mocks.js',
                        'bower_components/angular-animate/angular-animate.js',
                        'bower_components/angular-local-storage/dist/angular-local-storage.min.js',
                        'bower_components/angular-bootstrap/ui-bootstrap.min.js',
                        'bower_components/moment/moment.js',
                        'bower_components/angular-jwt/dist/angular-jwt.min.js',
                        'bower_components/angular-cookies/angular-cookies.min.js',
                        'js/helpers/scroll-to-view/scroll-to-view-directive.js',
                        'js/helpers/textarea-hashtag/smart-textarea-directive.js',
                        'js/helpers/outside-click/outside-click-directive.js',
                        'config.js',

                        // Testing Services
                        'js/services/services-module.js',
                        'js/services/error-service/error-service.js',

                        'js/services/questions-service/questions-service.js',
                        'js/services/ask-question-service/ask-question-service.js',
                        'js/services/answer-service/answers-service.js',
                        'js/services/tags-service/tags-service.js',
                        'js/services/voting/voting-service.js',

                        'js/services/current-user-service/current-user-service.js',

                        'js/services/ask-question-service/ask-question-service-test.js',
                        'js/services/questions-service/question-service-test.js',
                        'js/services/answer-service/answers-service-test.js',
                        'js/services/voting/voting-service-test.js',

                        //Testing Filters
                        'js/helpers/time-filter/time-filter.js',
                        'js/helpers/time-filter/time-filter-test.js',

                        // Testing Controllers
                        'js/modules/ask-question/ask-question-module.js',
                        'js/modules/ask-question/ask-question-controller.js',
                        'js/modules/ask-question/ask-question-controller-test.js',

                        'js/modules/answers/answers-module.js',
                        'js/modules/answers/answers-controller.js',
                        'js/modules/answers/answers-controller-test.js',

                        'js/modules/questions/questions-module.js',
                        'js/modules/questions/questions/questions-controller.js',
                        'js/modules/questions/questions/questions-controller-test.js',

                        'js/modules/questions/questions-unanswered/questions-unanswered-controller.js',
                        'js/modules/questions/questions-unanswered/questions-unanswered-controller-test.js',

                        'js/modules/questions/questions-user/questions-user-controller.js',
                        'js/modules/questions/questions-user/questions-user-controller-test.js',

                        'js/modules/search-questions/search-questions-module.js',
                        'js/modules/search-questions/search-questions-controller.js',
                        'js/modules/search-questions/search-questions-controller-test.js'
                    ]
                }
            }
        },
        concat: {
            js: { 
                src: [
                    'js/app.js',
                    'config.js',
                    // Load module definition first
                    'js/components/**/*module.js',
                    'js/components/**/*.js',
                    
                    // Load module definition first
                    'js/modules/**/*module.js',
                    'js/modules/**/*.js',

                    // Load module definition first
                    'js/services/services-module.js',
                    'js/services/**/*.js',

                    'js/controllers/*.js',
                    'js/helpers/**/*.js',

                    // Ignore any test files
                    '!**/*test.js'
                ],
                dest: 'dist/min/app.js'
            }
        },
        uglify: {
            js: { 
                src: ['dist/min/app.js'],
                dest: 'dist/app.min.js'
            }
        },
        'string-replace': {
            inline: {
                files: {
                    'index.html': 'index.html'
                },
                options: {
                    replacements: [
                        {
                            pattern: '<!--start PROD imports',
                            replacement: '<!--start PROD imports-->'
                        },
                        {
                            pattern: 'end PROD imports-->',
                            replacement: '<!--end PROD imports-->'
                        },
                        {
                            pattern: '<!--start DEV imports-->',
                            replacement: '<!--start DEV imports'
                        },
                        {
                            pattern: '<!--end DEV imports-->',
                            replacement: 'end DEV imports-->'
                        }
                    ]
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    'index.html': 'index.html'
                }
            }
        },
        ngconstant: {
            // Options for all targets
            options: {
                name: 'appConfig'
            },
            // Environment targets
            localDev: {
                options: {
                    dest: 'config.js'
                },
                constants: {
                    ENV: {
                        name: 'local',
                        apiEndpoint: 'https://questionsapi.amzingsandbox.com/api/',
                        authEndpoint: 'https://restapi.amzingsandbox.com/rest/v2/auth/login',
                        refreshEndpoint: 'https://sandbox.amazingbeta.com/rest/v2/auth/refresh-token',
                        campusUrl: 'http://campus.amzingsandbox.com/',
                        campusCookieUrl: '.amzingsandbox.com',
                        campusCookieDTL: 375,
                        legacyCommunityUrl: 'http://community.amazingbeta.com/',
                        supportUrl: 'https://support.amazing.com'
                    }
                }
            },
            development: {
                options: {
                    dest: 'config.js'
                },
                constants: {
                    ENV: {
                        name: 'development',
                        apiEndpoint: 'https://questionsapi.amzingsandbox.com/api/',
                        authEndpoint: 'https://restapi.amzingsandbox.com/rest/v2/auth/login',
                        refreshEndpoint: 'https://restapi.amzingsandbox.com/rest/v2/auth/refresh-token',
                        campusUrl: 'http://campus.amzingsandbox.com/',
                        campusCookieUrl: '.amzingsandbox.com',
                        campusCookieDTL: 375,
                        legacyCommunityUrl: 'http://community.amazingbeta.com/',
                        supportUrl: 'https://support.amazing.com'
                    }
                }
            },
            qa: {
                options: {
                    dest: 'config.js'
                },
                constants: {
                    ENV: {
                        name: 'QA',
                        apiEndpoint: 'https://questionsapi.amazingqa.com/api/',
                        authEndpoint: 'https://restapi.amazingbeta.com/rest/v2/auth/login',
                        refreshEndpoint: 'https://restapi.amazingbeta.com/rest/v2/auth/refresh-token',
                        campusUrl: 'http://campus.amzingsandbox.com/',
                        campusCookieUrl: '.amzingsandbox.com',
                        campusCookieDTL: 375,
                        legacyCommunityUrl: 'http://community.amazingbeta.com/',
                        supportUrl: 'https://support.amazing.com'
                    }
                }
            },
            uat: {
                options: {
                    dest: 'config.js'
                },
                constants: {
                    ENV: {
                        name: 'UAT',
                        apiEndpoint: 'https://questionsapi.amazingbeta.com/api/',
                        authEndpoint: 'https://restapi.amazing.com/rest/v2/auth/login',
                        refreshEndpoint: 'https://restapi.amazing.com/rest/v2/auth/refresh-token',
                        campusUrl: 'https://http://www.amazingbeta.com/',
                        campusCookieUrl: '.amazingbeta.com',
                        campusCookieDTL: 375,
                        legacyCommunityUrl: 'http://community.amazingbeta.com/',
                        supportUrl: 'https://support.amazing.com'
                    }
                }
            },
            production: {
                options: {
                    dest: 'config.js'
                },
                constants: {
                    ENV: {
                        name: 'production',
                        apiEndpoint: 'https://questionsapi.amazing.com/api/',
                        authEndpoint: 'https://restapi.amazing.com/rest/v2/auth/login',
                        refreshEndpoint: 'https://restapi.amazing.com/rest/v2/auth/refresh-token',
                        campusUrl: 'https://www.amazing.com/',
                        campusCookieUrl: '.amazing.com',
                        campusCookieDTL: 375,
                        legacyCommunityUrl: 'http://community.amazing.com/',
                        supportUrl: 'https://support.amazing.com'
                    }
                }
            }
        }

    });

    // Default task
    grunt.registerTask('default', ['less', 'watch']);
    
    // Testing Tasks
    grunt.registerTask('e2e-tests', ['protractor:e2e']);
    grunt.registerTask('unit-tests', ['ngconstant:development','jshint','karma']);
    
    // Build processes for Production
    grunt.registerTask('production-build', ['ngconstant:production','concat','uglify', 'string-replace', 'htmlmin']);
    grunt.registerTask('dev-build', ['ngconstant:development']);
    grunt.registerTask('uat-build', ['ngconstant:uat']);
    grunt.registerTask('qa-build', ['ngconstant:qa']);
    grunt.registerTask('local-build', ['ngconstant:localDev']);

};
