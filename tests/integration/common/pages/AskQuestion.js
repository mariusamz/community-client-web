/**
 * Created by garyfox on 26/08/2016.
 */
/*jshint -W109 */
var helpers = require('../helpers.js');
var data = require('../data.json');
var askQuestionObjects = require('../../common/objects/AskQuestionObjects.js');
var viewQuestionWithAnswerObjects = require('../../common/objects/ViewQuestionWithAnswerObjects.js');

var AskQuestionHome = function () {
    this.dismissVideoModal = function () {
        helpers.checkElementExists(askQuestionObjects.divVideoModal(), 10);
        browser.executeScript(data.dismissVideoModal);
        return this;
    };
    this.askSuccessfulQuestion = function (questionTitle,questionDetails) {
        helpers.checkElementExistAndSendKeys(questionTitle, askQuestionObjects.txtQuestionHeader(), 10);
        helpers.checkElementExistAndSendKeys(questionDetails, askQuestionObjects.txtQuestionDetail(), 10);
        helpers.checkElementExistAndClick(askQuestionObjects.btnPostQuestion(), 10);
        return require('./ViewQuestionWithAnswer.js');
    };
    this.validateUnsuccessfulQuestion = function () {
        this.verifyValidationForMinimumCharacters();
        this.verifyValidationFor20Tags();
        return this;

    };
    this.verifyValidationForMinimumCharacters = function () {
        helpers.checkElementExistAndSendKeys(data.minQuestionTitle, askQuestionObjects.txtQuestionHeader(), 10);
        helpers.checkElementExistAndClick(askQuestionObjects.btnPostQuestion(), 10);
        helpers.checkElementExists(askQuestionObjects.divBorderError(), 10);
        expect(element(askQuestionObjects.txtMinCharactersError()).getText()).toEqual(data.minCharactersError);
        return this;
    };
    this.verifyValidationFor20Tags = function () {
        helpers.checkElementExistAndSendKeys(data.questionTitle10Tags, askQuestionObjects.txtQuestionHeader(), 10);
        helpers.checkElementExistAndSendKeys(data.questionBody10Tags, askQuestionObjects.txtQuestionDetail(), 10);
        helpers.checkElementExistAndClick(askQuestionObjects.btnPostQuestion(), 10);
        helpers.checkElementExists(askQuestionObjects.txtInline20Tags(), 10);
        helpers.checkElementExists(askQuestionObjects.txtInline5Tags(), 10);
        helpers.verifyURLContainsSequence(data.urls.askQuestion, 5);
        helpers.checkElementDoesNotExist(viewQuestionWithAnswerObjects.containerQuestionsWithAnswers(), 10);
        return this;
    };

};
module.exports = new AskQuestionHome();
