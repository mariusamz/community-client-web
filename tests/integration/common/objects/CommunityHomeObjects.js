/*jshint -W109 */

var CommunityHomeObject = function () {
    this.divFooter = function () {
        return by.id('footer');
    };
    this.txtSearchQuestion = function () {
        return by.className('form-control');
    };
    this.lnkAskAQuestion = function () {
        return by.xpath("//a[@ui-sref='ask']");
    };
    this.txtUser = function (user) {
        return by.xpath("//li[text()='" + user + "']");
    };

};
module.exports = new CommunityHomeObject();
