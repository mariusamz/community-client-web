/**
 * Created by garyfox on 26/08/2016.
 * This is for the Ask A Question Spec. Tests Run Are (IE,Chrome,Firefox,Safari) :
 * Ask Sucessful Question
 * Validation For Minimum Characters,Question With No Tags
 */
var communityHome = require('../pages/CommunityHome.js');
var helpers = require('../../common/helpers.js');
var logger = require('../../common/log.js');
var data = require('../../common/data.json');
describe('Ask A Question Functionality', function () {
    beforeEach(function () {
        browser.get(browser.baseUrl);
    });
    it('Ask Successful Question With Tags', function () {
        communityHome
            .clickAskAQuestion()
            .askSuccessfulQuestion(data.successfulQuestionTitle, data.successfulQuestionDetail)
            .verifyOnQuestionWithAnswerPage();
    });
    it('Ask Successful Question Without Tags', function () {
        communityHome
            .clickAskAQuestion()
            .askSuccessfulQuestion(data.questionTitleNoTags, data.questionBodyNoTags)
            .verifyOnQuestionWithAnswerPage();
    });
    it('Ask Unsuccessful Question', function () {
        communityHome.clickAskAQuestion().validateUnsuccessfulQuestion();
    });
    afterEach(function () {
        if (browser.params.questionID !== 0 && browser.params.questionID !== undefined) {
            var dbSettings = require('../../common/dbqueries.js')(browser.params.dbDetails);
            var connection = dbSettings.connect();
            helpers.removeQuestionById(dbSettings, browser.params.questionID);
            browser.params.questionID = 0;
            browser.sleep(2000).then(function () {
                logger.log(data.loggingLevel.INFO, 'Done Deleting DB Entries');
                connection.end();
            });
        }
    });

});
