/**
 * Created by garyfox on 22/08/2016.
 */
var helpers = require('../../common/helpers.js');
var SearchResults = function () {
    this.verifyOnSearchResultsPage = function () {
        helpers.verifyURLContainsSequence('results', 5);
        return this;
    };


};
module.exports = new SearchResults();